<?php

namespace Redandmoon\Tests\Unit;

use PHPUnit\Framework\TestCase;
use Redandmoon\Designpatterns\Budget;
use Redandmoon\Designpatterns\BudgetsList;

class BudgetsListTest extends TestCase
{
    public function testBudgetList()
    {
        $order1 = $this->instanceOfOrder(10, 1000);
        $order2 = $this->instanceOfOrder(1, 1000);
        $order3 = $this->instanceOfOrder(7, 300);

        $order1->approve();

        $order2->disapprove();

        $order3->approve();
        $order3->finalize();

        $budgetsList = new BudgetsList();
        $budgetsList->addBudgets($order1);
        $budgetsList->addBudgets($order2);
        $budgetsList->addBudgets($order3);
    }

    private function instanceOfOrder(int $quantityOfItems, float $value)
    {
        $order = new Budget();

        $order->setQuantityOfItems($quantityOfItems);
        $order->setInvestmentValue($value);

        return $order;
    }
}