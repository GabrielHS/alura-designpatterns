<?php

namespace Redandmoon\Tests\Unit;

use PHPUnit\Framework\TestCase;
use Redandmoon\Designpatterns\Budget;
use Redandmoon\Designpatterns\DiscountCalculator;

/**
 * Regra de Negocio
 * Se tem mais de 5 itens desconto de 10%
 * Se tem menos de 5 Itens e o valor do orçamento é maior que 500 desconto de 5%
 * Se não satifaz nenhuma condição acima desconto de 0%
 */
class DiscountCalculatorTest extends TestCase
{
    private DiscountCalculator $discountCalculator;
    private Budget $budget;

    protected function setUp(): void
    {
        parent::setUp();
        $budget = new Budget();
        $discountCalculator = new DiscountCalculator();
        $this->budget = $budget;
        $this->discountCalculator = $discountCalculator;

    }

    public function testWithoutDiscont(): void
    {
        $this->budget->setQuantityOfItems(1);
        $this->budget->setInvestmentValue(100);

        $discountExpected = 0;
        $discountActual = $this->discountCalculator
            ->calculteDiscount($this->budget);

        $this->assertEquals($discountExpected, $discountActual);
    }

    public function testDiscountForMoreThan5Items(): void
    {
        $this->budget->setQuantityOfItems(7);
        $this->budget->setInvestmentValue(100);

        $value = $this->budget->getInvestmentValue();

        $discountExpected = $value * 0.1;
        $discountActual = $this->discountCalculator
            ->calculteDiscount($this->budget);

        $this->assertEquals($discountExpected, $discountActual);
    }

    public function testDiscountForValueMoreThan500(): void
    {
        $this->budget->setQuantityOfItems(1);
        $this->budget->setInvestmentValue(1000);

        $value = $this->budget->getInvestmentValue();

        $discountExpected = $value * 0.05;
        $discountActual = $this->discountCalculator
            ->calculteDiscount($this->budget);

        $this->assertEquals($discountExpected, $discountActual);
    }
}