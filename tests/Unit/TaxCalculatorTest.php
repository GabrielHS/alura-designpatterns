<?php

namespace Redandmoon\Tests\Unit;

use PHPUnit\Framework\TestCase;
use Redandmoon\Designpatterns\Budget;
use Redandmoon\Designpatterns\Tax\Icms;
use Redandmoon\Designpatterns\Tax\Icpp;
use Redandmoon\Designpatterns\Tax\Ikcv;
use Redandmoon\Designpatterns\Tax\Iss;
use Redandmoon\Designpatterns\TaxCalculator;

class TaxCalculatorTest extends TestCase
{
    private TaxCalculator $taxCalculator;
    private Budget $budget;

    public function setUp(): void
    {
        $this->budget = new Budget();
        $this->taxCalculator = new TaxCalculator();
    }

    public function testCalculateTaxIcms(): void
    {
        $icms = new Icms();
        $tax = 0.1;
        $this->budget->setInvestmentValue(100);

        $taxExpected = $this->budget->getInvestmentValue() * $tax;
        $taxActual = $this->taxCalculator
            ->calculateTax($this->budget, $icms);

        $this->assertEquals($taxExpected, $taxActual);
    }

    public function testCalculateTaxIss(): void
    {
        $iss = new Iss();
        $tax = 0.06;
        $this->budget->setInvestmentValue(100);

        $taxExpected = $this->budget->getInvestmentValue() * $tax;
        $taxActual = $this->taxCalculator
            ->calculateTax($this->budget, $iss);

        $this->assertEquals($taxExpected, $taxActual);
    }

    public function testCalculateTaxIcppMaxRate(): void
    {
        $icpp = new Icpp();
        $tax = 0.03;
        $this->budget->setInvestmentValue(1000);

        $taxExpected = $this->budget->getInvestmentValue() * $tax;
        $taxActual = $this->taxCalculator
            ->calculateTax($this->budget, $icpp);

        $this->assertEquals($taxExpected, $taxActual);
    }

    public function testCalculateTaxIcppMinRate(): void
    {
        $icpp = new Icpp();
        $tax = 0.02;
        $this->budget->setInvestmentValue(500);

        $taxExpected = $this->budget->getInvestmentValue() * $tax;
        $taxActual = $this->taxCalculator
            ->calculateTax($this->budget, $icpp);

        $this->assertEquals($taxExpected, $taxActual);
    }

    public function testCalculateTaxIkcvMaxRate(): void
    {
        $ikcv = new Ikcv();
        $tax = 0.04;
        $this->budget->setInvestmentValue(1000);
        $this->budget->setQuantityOfItems(5);

        $taxExpected = $this->budget->getInvestmentValue() * $tax;
        $taxActual = $this->taxCalculator
            ->calculateTax($this->budget, $ikcv);

        $this->assertEquals($taxExpected, $taxActual);
    }

    public function testCalculateTaxIkcvMinRate(): void
    {
        $ikcv = new Ikcv();
        $tax = 0.025;
        $this->budget->setInvestmentValue(1000);
        $this->budget->setQuantityOfItems(2);

        $taxExpected = $this->budget->getInvestmentValue() * $tax;
        $taxActual = $this->taxCalculator
            ->calculateTax($this->budget, $ikcv);

        $this->assertEquals($taxExpected, $taxActual);
    }


}

