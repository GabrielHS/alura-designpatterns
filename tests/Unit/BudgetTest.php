<?php

namespace Redandmoon\Tests\Unit;

use PHPUnit\Framework\TestCase;
use Redandmoon\Designpatterns\Budget;
use Redandmoon\Designpatterns\BudgetStates\Approved;
use Redandmoon\Designpatterns\BudgetStates\BudgetState;
use Redandmoon\Designpatterns\BudgetStates\Disapproved;
use Redandmoon\Designpatterns\BudgetStates\Finished;
use Redandmoon\Designpatterns\BudgetStates\InApproved;

class BudgetTest extends TestCase
{
    private Budget $budget;
    private float $value;
    private float $quantityOfItems;

    protected function setUp(): void
    {
        parent::setUp();

        $this->value = 1000;
        $this->quantityOfItems = 5;

        $budget = new Budget();
        $budget->setInvestmentValue($this->value);
        $budget->setQuantityOfItems($this->quantityOfItems);
        $this->budget = $budget;
    }

    public function testGetValue(): void
    {
        $value = $this->budget->getInvestmentValue();
        $this->assertEquals($this->value, $value);
    }

    public function testGetQuantityOfItems(): void
    {
        $quantityOfItems = $this->budget->getQuantityOfItems();
        $this->assertEquals($this->quantityOfItems, $quantityOfItems);
    }

    public function testWhenTheBudgetIsCreatedWithStatusInApproved(): void
    {
        $this->assertInstanceOf(InApproved::class, $this->budget->getStatus());
    }

    public function testChanceStatus():void
    {
        $this->budget->changeStatus(new Approved);
        $this->assertInstanceOf(Approved::class, $this->budget->getStatus());

        $this->budget->changeStatus(new DisApproved);
        $this->assertInstanceOf(Disapproved::class, $this->budget->getStatus());

        $this->budget->changeStatus(new Finished);
        $this->assertInstanceOf(Finished::class, $this->budget->getStatus());
    }

    public function testBudgetStatesInApprovedChangeToDisapproved(): void
    {
        $this->budget->disapprove();
        $this->assertInstanceOf(Disapproved::class, $this->budget->getStatus());
    }

    public function testBudgetStatesInApprovedChangeToApproved(): void
    {
        $this->budget->approve();
        $this->assertInstanceOf(Approved::class, $this->budget->getStatus());
    }

    public function testBudgetStatesInApprovedChangeToFinished(): void
    {
        $this->expectExceptionMessage('Este Orçamento não pode ser finalizado!');
        $this->budget->finalize();
    }

    public function testBudgetStatesApprovedChangeToDisapproved(): void
    {
        $this->budget->changeStatus(New Approved);

        $this->expectExceptionMessage('Este Orçamento não pode ser desaprovado!');
        $this->budget->disapprove();
    }

    public function testBudgetStatesApprovedChangeToFinished(): void
    {
        $this->budget->changeStatus(New Approved);

        $this->budget->finalize(New Finished);
        $this->assertInstanceOf(Finished::class, $this->budget->getStatus());
    }

    public function testBudgetStatesApprovedChangeToApproved(): void
    {
        $this->budget->changeStatus(New Approved);

        $this->expectExceptionMessage('Este Orçamento não pode ser aprovado!');
        $this->budget->approve();

    }
    public function testBudgetStatesDisapprovedChangeToApproved(): void
    {
        $this->budget->changeStatus(New Disapproved);

        $this->expectExceptionMessage('Este Orçamento não pode ser aprovado!');
        $this->budget->approve();
    }

    public function testBudgetStatesDisapprovedChangeToFinished(): void
    {
        $this->budget->changeStatus(New Disapproved);

        $this->budget->finalize();
        $this->assertInstanceOf(Finished::class, $this->budget->getStatus());
    }

    public function testBudgetStatesFinishedChangeToApproved(): void
    {
        $this->budget->changeStatus(New Finished);

        $this->expectExceptionMessage('Este Orçamento não pode ser aprovado!');
        $this->budget->Approve();
    }

    public function testBudgetStatesFinishedChangeToDisapproved(): void
    {
        $this->budget->changeStatus(New Finished);

        $this->expectExceptionMessage('Este Orçamento não pode ser desaprovado!');
        $this->budget->Disapprove();
    }

    public function testBudgetStatesFinishedChangeToFinished(): void
    {
        $this->budget->changeStatus(New Finished);

        $this->expectExceptionMessage('Este Orçamento não pode ser finalizado!');
        $this->budget->finalize();
    }


}