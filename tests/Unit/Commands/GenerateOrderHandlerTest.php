<?php

namespace Redandmoon\Tests\Unit\Commands;

use PHPUnit\Framework\TestCase;
use Redandmoon\Designpatterns\Actions\GenerateOrders\CreateOrdersInDB;
use Redandmoon\Designpatterns\Actions\GenerateOrders\GenerateOrdersLog;
use Redandmoon\Designpatterns\Actions\GenerateOrders\SendOrdersByEmail;
use Redandmoon\Designpatterns\Commands\GenerateOrder;
use Redandmoon\Designpatterns\Commands\GenerateOrderHandler;

class GenerateOrderHandlerTest extends TestCase
{
    public function testGenerationOrders(): void
    {
        $orderValue = 1000;
        $quantityOfItems = 10;
        $clientName = "Gabriel Henrique";

        $generateOrder = new GenerateOrder(
            $orderValue,
            $quantityOfItems,
            $clientName
        );
        $generateOrderHandler = new GenerateOrderHandler();

        $generateOrderHandler->addActionOnOrderGeneration(new CreateOrdersInDB());
        $generateOrderHandler->addActionOnOrderGeneration(new SendOrdersByEmail());
        $generateOrderHandler->addActionOnOrderGeneration(new GenerateOrdersLog());

        $response = $generateOrderHandler->execute($generateOrder);

        $this->assertEquals(200, $response);
    }
}