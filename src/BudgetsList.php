<?php

namespace Redandmoon\Designpatterns;

class BudgetsList implements \IteratorAggregate
{
    private array $budgets;

    public function __construct()
    {
        $this->budgets = [];
    }

    public function addBudgets(Budget $budget)
    {
        $this->budgets[] = $budget;
    }

    public function budgetsList(): array
    {
        return $this->budgets;
    }

    public function getIterator()
    {
         return new \ArrayIterator($this->budgets);
    }

}