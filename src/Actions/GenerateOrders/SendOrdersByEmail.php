<?php

namespace Redandmoon\Designpatterns\Actions\GenerateOrders;

use Redandmoon\Designpatterns\Order;

class SendOrdersByEmail implements ActionAfterCreatingOrderInterface
{
    public function execute(Order $order): void
    {
        //echo "Enviando Email do pedido gerado";
    }
}