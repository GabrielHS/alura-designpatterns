<?php

namespace Redandmoon\Designpatterns\Actions\GenerateOrders;

use Redandmoon\Designpatterns\Order;

class GenerateOrdersLog implements ActionAfterCreatingOrderInterface
{
     public function execute(Order $order): void
    {
        //echo "Gerando Log do pedido";
    }
}