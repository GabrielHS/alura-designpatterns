<?php

namespace Redandmoon\Designpatterns\Actions\GenerateOrders;

use Redandmoon\Designpatterns\Order;

interface ActionAfterCreatingOrderInterface
{
    public function execute(Order $order): void;
}