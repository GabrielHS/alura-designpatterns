<?php

namespace Redandmoon\Designpatterns\Actions\GenerateOrders;

use Redandmoon\Designpatterns\Order;

class CreateOrdersInDB implements ActionAfterCreatingOrderInterface
{
    public function execute(Order $order): void
    {
        //echo "Salvando pedido no Banco de Dados";
    }
}