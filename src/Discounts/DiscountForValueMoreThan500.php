<?php

namespace Redandmoon\Designpatterns\Discounts;

use Redandmoon\Designpatterns\Budget;

class DiscountForValueMoreThan500 extends Discount
{
    public function calculateDiscount(Budget $budget): float
    {
        if ($budget->getInvestmentValue() > 500) {
            return $budget->getInvestmentValue() * 0.05;
        }

        return $this->nextDiscount->calculateDiscount($budget);
    }
}