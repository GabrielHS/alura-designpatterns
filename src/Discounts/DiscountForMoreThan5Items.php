<?php

namespace Redandmoon\Designpatterns\Discounts;

use Redandmoon\Designpatterns\Budget;

class DiscountForMoreThan5Items extends Discount
{
    public function calculateDiscount(Budget $budget): float
    {
        if ($budget->getQuantityOfItems() > 5) {
            return $budget->getInvestmentValue() * 0.1;
        }

        return $this->nextDiscount->calculateDiscount($budget);
    }
}