<?php

namespace Redandmoon\Designpatterns\Discounts;

use Redandmoon\Designpatterns\Budget;

class WithoutDiscount extends Discount
{
    public function __construct()
    {
        parent::__construct(null);
    }

    public function calculateDiscount(Budget $budget): float
    {
        return 0;
    }
}