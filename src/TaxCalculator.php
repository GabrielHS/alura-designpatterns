<?php

namespace Redandmoon\Designpatterns;

use Redandmoon\Designpatterns\Tax\TaxInterface;

class TaxCalculator
{
    public function calculateTax(Budget $budget, TaxInterface $tax): float
    {
        return $tax->calculateTax($budget);
    }
}