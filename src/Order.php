<?php

namespace Redandmoon\Designpatterns;

class Order
{
    private string $clientName;
    private \DateTimeInterface $formalizationDate;
    public Budget $budget;

    public function getClientName(): string
    {
        return $this->clientName;
    }

    public function setClientName(string $clientName): void
    {
        $this->clientName = $clientName;
    }

    public function formalizationDate(): \DateTimeInterface
    {
        return $this->formalizationDate;
    }

    public function setFormalizationDate(\DateTimeInterface $formalizationDate): void
    {
        $this->formalizationDate = $formalizationDate;
    }

    public function budget(): Budget
    {
        return $this->budget;
    }

    public function setBudget(Budget $budget): void
    {
        $this->budget = $budget;
    }
}