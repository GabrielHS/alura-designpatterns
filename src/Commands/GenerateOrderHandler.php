<?php

namespace Redandmoon\Designpatterns\Commands;

use Redandmoon\Designpatterns\Actions\GenerateOrders\ActionAfterCreatingOrderInterface;
use Redandmoon\Designpatterns\Actions\GenerateOrders\CreateOrdersInDB;
use Redandmoon\Designpatterns\Actions\GenerateOrders\GenerateOrdersLog;
use Redandmoon\Designpatterns\Actions\GenerateOrders\SendOrdersByEmail;
use Redandmoon\Designpatterns\Budget;
use Redandmoon\Designpatterns\Order;

class GenerateOrderHandler
{
    /**
     * @var ActionAfterCreatingInterface
     */
    private array $actionsAfterGenerateOrders = [];

    public function __construct(/* PedidoRepository, MailService */)
    {

    }

    public function addActionOnOrderGeneration(ActionAfterCreatingOrderInterface $action)
    {
         $this->actionsAfterGenerateOrders[] = $action;
    }

    public function execute(GenerateOrder $generateOrder)
    {
        $budget = new Budget();
        $budget->setQuantityOfItems($generateOrder->getQuantityOfItems());
        $budget->setInvestmentValue($generateOrder->getBudgetValue());

        $order = new Order();
        $order->setFormalizationDate(new \DateTimeImmutable());
        $order->setClientName($generateOrder->getClientName());
        $order->budget = $budget;

        foreach ($this->actionsAfterGenerateOrders as $action) {
            $action->execute($order);
        }
        return 200; // para teste
    }
}