<?php

namespace Redandmoon\Designpatterns\Commands;


interface CommandInterface
{
    public function execute();
}