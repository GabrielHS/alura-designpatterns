<?php

namespace Redandmoon\Designpatterns\Commands;

use Redandmoon\Designpatterns\Budget;
use Redandmoon\Designpatterns\Order;

class GenerateOrder
{
    private float $budgetValue;
    private int $quantityOfItems;
    private string $clientName;

    public function __construct(
        float $budgetValue,
        int $quantityOfItems,
        string $clientName
    )
    {
        $this->budgetValue = $budgetValue;
        $this->quantityOfItems = $quantityOfItems;
        $this->clientName = $clientName;
    }

    public function getClientName(): string
    {
        return $this->clientName;
    }

    public function setClientName(string $clientName): void
    {
        $this->clientName = $clientName;
    }

    public function getQuantityOfItems(): int
    {
        return $this->quantityOfItems;
    }

    public function setQuantityOfItems(int $quantityOfItems): void
    {
        $this->quantityOfItems = $quantityOfItems;
    }

    public function getBudgetValue(): float
    {
        return $this->budgetValue;
    }

    public function setBudget(float $budgetValue): void
    {
        $this->budget = $budgetValue;
    }


}