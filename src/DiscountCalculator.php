<?php

namespace Redandmoon\Designpatterns;

use Redandmoon\Designpatterns\Discounts\DiscountForValueMoreThan500;
use Redandmoon\Designpatterns\Discounts\DiscountForMoreThan5Items;
use Redandmoon\Designpatterns\Discounts\WithoutDiscount;

class DiscountCalculator
{
    /**
     * Retorna o valor do desconto
     * Se tem mais de 5 itens desconto de 10%
     * Se tem menos de 5 Itens e o valor do orçamento é maior que 500 desconto de 5%
     * Se não satifaz nenhuma condição acima desconto de 0%
     *
     * @param Budget $budget
     * @return float
     */
    public function calculteDiscount(Budget $budget):float
    {
        $withoutDiscount = new WithoutDiscount();
        $DiscountForValueMoreThan500 = new DiscountForValueMoreThan500($withoutDiscount);
        $discountForMoreThan5Items = new DiscountForMoreThan5Items($DiscountForValueMoreThan500);

        $chainDiscount = $discountForMoreThan5Items;

        return $chainDiscount->calculateDiscount($budget);
    }
}