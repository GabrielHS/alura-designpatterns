<?php

namespace Redandmoon\Designpatterns\Tax;

use Redandmoon\Designpatterns\Budget;

class Icpp extends TaxWith2Rates
{
    protected function mustApplyMaximumTax(Budget $budget): bool
    {
        return $budget->getInvestmentValue() > 500;
    }

    protected function calculatesMaximumTax(Budget $budget): float
    {
        return $budget->getInvestmentValue() * 0.03;
    }

    protected function calculatesMinimumTax(Budget $budget): float
    {
        return $budget->getInvestmentValue() * 0.02;
    }
}