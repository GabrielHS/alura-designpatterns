<?php

namespace Redandmoon\Designpatterns\Tax;

use Redandmoon\Designpatterns\Budget;

class Iss implements TaxInterface
{
    public function calculateTax(Budget $budget): float
    {
        return $budget->getInvestmentValue() * 0.06;
    }
}