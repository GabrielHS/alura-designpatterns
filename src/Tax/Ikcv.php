<?php

namespace Redandmoon\Designpatterns\Tax;

use Redandmoon\Designpatterns\Budget;

class Ikcv extends TaxWith2Rates
{
    protected function mustApplyMaximumTax(Budget $budget): bool
    {
        return $budget->getInvestmentValue() > 300 && $budget->getQuantityOfItems() > 3;
    }

    protected function calculatesMaximumTax(Budget $budget): float
    {
        return $budget->getInvestmentValue() * 0.04;
    }

    protected function calculatesMinimumTax(Budget $budget): float
    {
        return $budget->getInvestmentValue() * 0.025;
    }

}