<?php

namespace Redandmoon\Designpatterns\Tax;

use Redandmoon\Designpatterns\Budget;

class Icms implements TaxInterface
{
    public function calculateTax(Budget $budget): float
    {
        return $budget->getInvestmentValue() * 0.1;
    }
}