<?php

namespace Redandmoon\Designpatterns\Tax;

use Redandmoon\Designpatterns\Budget;

interface TaxInterface
{
    public function calculateTax(Budget $budget): float;
}