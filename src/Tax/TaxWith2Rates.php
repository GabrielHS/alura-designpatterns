<?php

namespace Redandmoon\Designpatterns\Tax;

use Redandmoon\Designpatterns\Budget;

abstract class TaxWith2Rates implements TaxInterface
{
    public function calculateTax(Budget $budget): float
    {
       if ($this->mustApplyMaximumTax($budget)) {
           return $this->calculatesMaximumTax($budget);
       }

       return $this->calculatesMinimumTax($budget);
    }

    abstract protected function mustApplyMaximumTax(Budget $budget): bool;
    abstract protected function calculatesMaximumTax(Budget $budget): float;
    abstract protected function calculatesMinimumTax(Budget $budget): float;
}