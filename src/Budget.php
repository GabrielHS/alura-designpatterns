<?php

namespace Redandmoon\Designpatterns;

use Redandmoon\Designpatterns\BudgetStates\BudgetState;
use Redandmoon\Designpatterns\BudgetStates\InApproved;

class Budget
{
    private float $value;
    private int $quantityOfItems;
    private BudgetState $status;

    public function __construct()
    {
        $this->status = new InApproved();
    }
    public function setInvestmentValue(float $value): void
    {
        $this->value = $value;
    }

    public function getInvestmentValue(): float
    {
        return $this->value;
    }

    public function setQuantityOfItems(int $quantityOfItems): void
    {
        $this->quantityOfItems = $quantityOfItems;
    }

    public function getQuantityOfItems(): float
    {
        return $this->quantityOfItems;
    }

    public function changeStatus(BudgetState $budgetState): BudgetState
    {
        return $this->status = $budgetState;
    }

    public function getStatus(): BudgetState
    {
        return $this->status;
    }

    public function applyExtraDiscount()
    {
        $this->value -= $this->status->calculateExtraDiscount($this);
    }

    public function approve()
    {
        $this->status->approve($this);
    }

    public function disapprove()
    {
        $this->status->disapprove($this);
    }

    public function finalize()
    {
        $this->status->finalize($this);
    }


}