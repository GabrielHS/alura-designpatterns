<?php

namespace Redandmoon\Designpatterns\BudgetStates;

use Redandmoon\Designpatterns\Budget;

class Disapproved extends BudgetState
{
    public function calculateExtraDiscount(Budget $budget): float
    {
        throw new \DomainException('Um orçamento reprovado não pode receber desconto');
    }

    public function finalize(Budget $budget)
    {
        $budget->changeStatus(new Finished);
    }
}