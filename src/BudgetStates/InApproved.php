<?php

namespace Redandmoon\Designpatterns\BudgetStates;

use Redandmoon\Designpatterns\Budget;

class InApproved extends BudgetState
{
    public function calculateExtraDiscount(Budget $budget): float
    {
        return $this->value * 0.05;
    }

    public function approve(Budget $budget)
    {
        $budget->changeStatus(new Approved);
    }

    public function disapprove(Budget $budget)
    {
        $budget->changeStatus(new Disapproved);
    }

}