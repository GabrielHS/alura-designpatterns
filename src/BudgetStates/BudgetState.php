<?php

namespace Redandmoon\Designpatterns\BudgetStates;

use Redandmoon\Designpatterns\Budget;

abstract class BudgetState
{
    /**
     * @throws \DomainException
     */
    abstract public function calculateExtraDiscount(Budget $budget): float;

    public function approve(Budget $budget)
    {
        throw new \DomainException('Este Orçamento não pode ser aprovado!');
    }

    public function disapprove(Budget $budget)
    {
        throw new \DomainException('Este Orçamento não pode ser desaprovado!');
    }

    public function finalize(Budget $budget)
    {
        throw new \DomainException('Este Orçamento não pode ser finalizado!');
    }



}