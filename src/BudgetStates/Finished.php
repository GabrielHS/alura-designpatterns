<?php

namespace Redandmoon\Designpatterns\BudgetStates;

use Redandmoon\Designpatterns\Budget;

class Finished extends BudgetState
{
    public function calculateExtraDiscount(Budget $budget): float
    {
        throw new \DomainException('Um orçamento finalizado não pode receber desconto');
    }
}