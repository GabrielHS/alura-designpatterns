<?php

namespace Redandmoon\Designpatterns\BudgetStates;

use Redandmoon\Designpatterns\Budget;

class Approved extends BudgetState
{
    public function calculateExtraDiscount(Budget $budget): float
    {
        return $this->value * 0.02;
    }

    public function finalize(Budget $budget)
    {
        $budget->changeStatus(new Finished);
    }
}